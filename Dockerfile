FROM debian:buster-slim
MAINTAINER Judd West (judd@stormdragondesigns.com.au)
RUN sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install m4 unzip zip build-essential wget curl tap-plugins swh-plugins libcap2 libselinux1 sudo bubblewrap -y && apt-get clean
RUN wget https://github.com/ocaml/opam/releases/download/2.0.3/opam-2.0.3-$(uname -m)-linux
RUN chmod a+x opam-2.0.3-$(uname -m)-linux
RUN mv opam-2.0.3-$(uname -m)-linux /usr/local/bin/opam
RUN adduser --disabled-password --gecos "" westj
RUN passwd -l westj
COPY ./westjsudo /etc/sudoers.d/westj
RUN mkdir -p /home/westj/music && mkdir -p /home/westj/id && mkdir -p /home/westj/config && mkdir -p /home/westj/logs && chown westj:westj /home/westj/*
RUN wget "https://ftp.osuosl.org/pub/xiph/releases/flac/flac-1.3.2.tar.xz"
RUN wget "http://prdownloads.sourceforge.net/mad/libmad-0.15.1b.tar.gz"
RUN tar -xvf libmad-0.15.1b.tar.gz
WORKDIR /libmad-0.15.1b
RUN sed -i.bak 's/-fforce-mem//g' configure
RUN sed -i.bak 's/-fforce-mem//g' configure.ac
RUN ./configure && make && make install
WORKDIR /
RUN tar -xvf flac-1.3.2.tar.xz
WORKDIR /flac-1.3.2
RUN ./configure && make && make install
RUN rm -rf libmad* libogg* flac*
RUN apt-get install -y rsync darcs mercurial git && apt-get clean
USER westj
RUN cd
RUN opam init --comp=4.05.0 --disable-sandboxing -y -a
RUN eval `opam config env`
RUN opam install depext -y
RUN eval `opam config env`
RUN opam depext --install -y taglib.0.3.3 mad.0.4.5 faad.0.4.0 fdkaac.0.2.1 lame.0.3.3 vorbis.0.7.1 cry.0.6.1 flac.0.1.4 opus.0.1.2 duppy.0.8.0 ssl liquidsoap.1.3.6 ladspa
USER root
RUN rm /etc/sudoers.d/westj
RUN apt-get remove -y build-essential wget m4 unzip zip rsync darcs mercurial && apt-get clean
USER westj
RUN cd ~
RUN eval `opam config env`
RUN chown westj:westj /home/westj/logs/
WORKDIR /home/westj/config
RUN touch liquidsoap.liq
WORKDIR /home/westj
COPY int-script/start-liquidsoap.sh /home/westj/start-liquidsoap.sh
STOPSIGNAL SIGTERM
ENTRYPOINT ["/home/westj/.opam/4.05.0/bin/liquidsoap", "/home/westj/config/liquidsoap.liq"] 