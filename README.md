# Fillydelphia Radio Liquidsoap Docker

## Using Liquidsoap 1.3.6

Build the docker

```
docker build -t fillyradio/ls:1.3.6 .
```

Put your config in config/liquidsoap.liq

Modify `run-docker.sh` to adjust the needed ports and volumes

Then run:

```
./run-docker.sh
```
