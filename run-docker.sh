#!/bin/bash

docker run -v $(pwd)/config/:/home/westj/config/ -v $(pwd)/logs:/home/westj/logs/ \
## Add ports as needed for harbour inputs
# -p 8080:8080 \
## Add mounts for music and jingles if desired
# -v /path/to/music/:/home/westj/music/ -v /path/to/jingles/:/home/westj/jingles \
--name=ls fillyradio/ls:1.3.6

